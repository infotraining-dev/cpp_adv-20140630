#include <iostream>
#include "stack.hpp"

using namespace std;

int main()
{
	int numbers[5] = {1, 2, 3, 4, 5};
	
	try
	{
		Stack<int> stackInt(5);
		
		stackInt.push(6);
		
		cout << stackInt.top() << endl;
		
		for (int i = 0; i < 5; ++i) {
			stackInt.push(numbers[i]);
		}
		
		for (int i = 0; i < 10; ++i)
		{
			cout << "Bierzący element na stosie: " << stackInt.top() << endl;
			stackInt.pop();
		}
	}
	catch (const std::exception& e)
	{
		cout << e.what() << endl;
	}
}
