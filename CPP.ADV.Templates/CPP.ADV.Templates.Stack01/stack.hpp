#ifndef STACK_HPP_
#define STACK_HPP_

#include <stdexcept>

template <class T>
class Stack
{
public:
	Stack(size_t capacity = 100);
	~Stack();
	void push(const T& elem);  // dodaje element na szczycie stosu
	void pop();					 // usuwa element ze szczytu stosu
	T top() const;              // zwraca element znajduj�cy si� na szczycie 
	
private:
	size_t capacity_;
	size_t size_;
	T* elems_;
};

template <typename T>
Stack<T>::Stack(size_t capacity) : capacity_(capacity), size_(0), elems_(NULL)
{
	elems_ = new T[capacity_];
}

template <typename T>
Stack<T>::~Stack()
{
	delete [] elems_;
}

template <typename T>
void Stack<T>::push(const T& elem)
{
	if (size_ == capacity_)
		throw std::out_of_range("Przepe�nienie stosu");
	
	elems_[size_++] = elem;
}

template <typename T>
void Stack<T>::pop()
{
	if (size_ == 0)
		throw std::out_of_range("Stos pusty");
	
	size_--;
}

template <typename T>
T Stack<T>::top() const
{
	if (size_ == 0)
		throw std::out_of_range("Stos pusty");
	return elems_[size_ - 1];
}

#endif /*STACK_HPP_*/
