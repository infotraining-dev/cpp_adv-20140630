#include <iostream>
#include "stack.hpp"

using namespace std;

int main()
{
	Stack<int> stackInt;
	
	stackInt.push(1);
	stackInt.push(2);
	stackInt.push(3);
	
	for (int i = 0; i < 3 ; ++i)
	{
		cout << stackInt.top() << endl;
		stackInt.pop();
	}
	
	cout << "--------------------------" << endl;
	
	Stack<double> stackDouble;
	
	stackDouble.push(5.0);
	stackDouble.push(6.0);
	stackDouble.push(9.6);
	stackDouble.push(11.5);
	
	stackInt = stackDouble;
	
	for (int i = 0; i < 4; ++i)
	{
		cout << stackInt.top() << endl;
		stackInt.pop();
	}
}
