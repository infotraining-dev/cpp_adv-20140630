#ifndef STACK_HPP_
#define STACK_HPP_

#include <deque>
#include <stdexcept>

template <typename T>
class Stack
{
private:
	std::deque<T> elems_;
public:
	void push(const T&);
	void pop();
	T top() const;
	bool empty() const
	{
		return elems_.empty();
	}
	
	// przypisanie stosu o elementach typu T2
	template <typename T2>
	Stack<T>& operator=(const Stack<T2>&);
};

template <typename T>
void Stack<T>::push(const T& elem)
{
	elems_.push_back(elem);
}

template <typename T>
void Stack<T>::pop()
{
	if (elems_.empty())
		throw std::out_of_range("Stack<>. Stos pusty");
	
	elems_.pop_back();
}

template <typename T>
T Stack<T>::top() const
{
	if (elems_.empty())
		throw std::out_of_range("Stack<>. Stos pusty");
	
	return elems_.back();
}

template <typename T>
	template <typename T2>
Stack<T>& Stack<T>::operator=(const Stack<T2>& source)
{
	if ( (void*)this == (void*)&source )  // uniknięcie przypisania do samego siebie
		return *this;
	
	Stack<T2> tmp(source);  // tworzy kopię przypisywanego stosu
	
	elems_.clear();  
	
	while (!tmp.empty())
	{
		elems_.push_front(tmp.top());
		tmp.pop();
	}
	
	return *this;
}

#endif /*STACK_HPP_*/
