#ifndef STACK_HPP_
#define STACK_HPP_

#include <vector>
#include <stdexcept>

template <typename T, template<typename, typename> class CONT, typename A = std::allocator<T> >
class Stack
{
private:
	CONT<T, A> elems_;
public:
	Stack();
	void push(const T& elem);
	void pop();
	T top() const;
};

template <typename T, template<typename, typename> class CONT, typename A >
Stack<T, CONT, A>::Stack()
{
}

template <typename T, template<typename, typename> class CONT, typename A >
void Stack<T, CONT, A>::push(const T& elem)
{
	elems_.push_back(elem);
}

template <typename T, template<typename, typename> class CONT, typename A >
void Stack<T, CONT, A>::pop()
{
	if (elems_.empty())
		throw std::out_of_range("Stack<>. Stos pusty");
	
	elems_.pop_back();
}

template <typename T, template<typename, typename> class CONT, typename A >
T Stack<T, CONT, A>::top() const
{
	if (elems_.empty())
		throw std::out_of_range("Stack<>. Stos pusty");
	
	return elems_.back();
}

#endif /*STACK_HPP_*/
