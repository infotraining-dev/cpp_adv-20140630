#include <iostream>
#include <vector>
#include <deque>
#include <stdexcept>
#include "stack.hpp"

using namespace std;

int main()
{
    try
    {
        Stack<const char*, vector> stackWords;

        stackWords.push("Ala");
        stackWords.push("ma");
        stackWords.push("kota");

        for (int i = 0; i < 4; ++i)
        {
            cout << stackWords.top() << endl;
            stackWords.pop();
        }
    }
    catch (const std::exception& e)
    {
        cout << e.what() << endl;
    }

    return 0;
}
