#include <iostream>
#include <string>
#include <functional>
#include <algorithm>
#include <vector>

using namespace std;

/*	prosty algorytm sortowania

    sort(tab[], rozmiar)
        for i = 0 to rozmiar-1:
            for j = i to rozmiar-1:
                if (tab[j] < tab[i]):
                    swap(tab[i], tab[j])
*/

class Person
{
    int id_;
    string name_;
public:
    Person(int id, string name) : id_(id), name_(name)
    {
    }

    bool operator<(const Person& other) const
    {
        return id_ < other.id_;
    }

    string name() const
    {
        return name_;
    }

    void print(ostream& out) const
    {
        out << "Person(id = " << id_ << ", name = " << name_ << ")";
    }
};

ostream& operator<<(ostream& out, const Person& p)
{
    p.print(out);

    return out;
}

class ComparePersonByName
{
public:
    bool operator()(const Person& p1, const Person& p2) const
    {
        return p1.name() < p2.name();
    }
};

/* 1 - napisa� uog�lniony algorytm sortuj�cy (szablon funkcji) */

template <typename T>
void slow_sort(T arr[], size_t size)
{
    for (size_t i = 0; i < size; ++i)
        for (size_t j = i; j < size; ++j)
            if (arr[j] < arr[i])
            {
                T tmp(arr[j]);
                arr[j] = arr[i];
                arr[i] = tmp;
            }
}

template <typename T, typename Comp>
void slow_sort(T arr[], size_t size, Comp comp)
{
    for (size_t i = 0; i < size; ++i)
        for (size_t j = i; j < size; ++j)
            if (comp(arr[j], arr[i]))
            {
                T tmp(arr[j]);
                arr[j] = arr[i];
                arr[i] = tmp;
            }
}

template <typename FwdIt, typename Comp>
void slow_sort(FwdIt b, FwdIt e, Comp comp)
{
    for (FwdIt i(b); i != e; ++i)
        for (FwdIt j(i); j != e; ++j)
            if (comp(*j, *i))
                swap(*i, *j);
}

template <typename FwdIt>
void slow_sort(FwdIt b, FwdIt e)
{
    for (FwdIt i(b); i != e; ++i)
        for (FwdIt j(i); j != e; ++j)
            if (*j < *i)
                swap(*i, *j);
}

/* 2 - napisa� szablon funkcji wypisuj�cej elementy kontenera na ekranie */

template <typename Iterator>
void print_container(Iterator start, Iterator end)
{
    cout << "[ ";
    for (Iterator it = start; it != end; ++it)
        cout << *it << " ";
    cout << "]" << endl;
}

bool my_greater(int a, int b)
{
    return a > b;
}

class MyGreater
{
public:
    bool operator()(int a, int b) const
    {
        return a > b;
    }
};

template <typename T, size_t N, size_t M>
pair<size_t, size_t> get_size(T (&arg)[N][M])
{
    static_assert(N == M, "Wymiary macierzy musz� by� takie same");

    return make_pair(N, M);
}



int main()
{
    int numbers[10] = { 9, 5, 1, 9, 5, 6, 8, 3, 2, 0 };

    int matrix[10][10];

    cout << "Size of matrix: "
         <<  get_size(matrix).first << "x" << get_size(matrix).second  << endl;

    sort(numbers, numbers + 10);

    print_container(numbers, numbers+10);

    string strings[5] =
        { string("1_jeden"), string("4_cztery"), string("2_dwa"), string("8_osiem"), string("7_siedem") };

    slow_sort(strings, 5);

    print_container(strings, strings+5);

    slow_sort(numbers, 10, greater<int>());

    print_container(numbers, numbers + 10);

    vector<int> vec1 = { 7, 2, 2, 9, 5, 1, 7, 2, 10 };

    slow_sort(vec1.begin(), vec1.end());

    print_container(vec1.begin(), vec1.end());

    Person people[] = { Person(4, "Kowalski"), Person(2, "Nijaki"), Person(6, "Anonim"), Person(1, "Nowak") };

    slow_sort(people, 4);

    print_container(people, people + 4);

    slow_sort(people, 4, ComparePersonByName());

    print_container(people, people + 4);

    return 0;
}
