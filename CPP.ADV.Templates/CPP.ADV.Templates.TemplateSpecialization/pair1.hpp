#ifndef PAIR1_HPP_
#define PAIR1_HPP_

template <typename T>
class Pair
{
private:
	T values_[2];
public:
	Pair(T first, T second);
	
	T max() const;
	
	void swap();
	
	const T& first() const
	{
		return values_[0];
	}
	
	const T& second() const
	{
		return values_[1];
	}
};

template <typename T>
Pair<T>::Pair(T first, T second)
{
	values_[0] = first;
	values_[1] = second;
}

template <typename T>
T Pair<T>::max() const
{
	return (values_[0] < values_[1]) ? values_[1] : values_[0];
}

template <typename T>
void Pair<T>::swap()
{
	T temp(values_[0]);
	values_[0] = values_[1];
	values_[1] = temp;	
}

template <typename T>
std::ostream& operator <<(std::ostream& out, const Pair<T>& p)
{
	out << "[" << p.first() << ", " << p.second() << "]";
	
	return out;
}

#endif /*PAIR1_HPP_*/

