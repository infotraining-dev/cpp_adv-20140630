#include <iostream>
#include <string.h>
#include "pair1.hpp"
#include "pair2.hpp"
#include "pair3.hpp"

using namespace std;

int main()
{
	Pair<int> p(1, 5);
	
	cout << p << endl;
	
	Pair<const char*> words("Ala", "ala");
	
	words.swap();
	
	cout << "max z " << words << " to " << words.max() << endl;
	
	
	string* str1 = new string("1_jeden");
	string* str2 = new string("2_dwa");
	
	Pair<string*> ptrsPair(str1, str2);
	
	cout << "max z ptrsPair: " << ptrsPair.max() << "\n";
	
	delete str1;
	delete str2;
	
	return 0;
}
