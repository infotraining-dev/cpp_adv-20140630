#ifndef PAIR2_HPP_
#define PAIR2_HPP_

#include <iostream>

template<>
class Pair<const char*>
{
	const char* first_;
	const char* second_;
	
public:
	Pair(const char* txt1, const char* txt2);
	
	const char* max() const;
	
	void swap();
	
	const char* first() const
	{
		return first_;
	}
	
	const char* second() const
	{
		return second_;
	}
};

Pair<const char*>::Pair(const char* txt1, const char* txt2)
{
	first_ = txt1;
	second_ = txt2;
}

const char* Pair<const char*>::max() const
{
	std::cout << "Dziala const char* Pair<const char*>::getMax() const" << std::endl;
	
	if (strcmp(first_, second_) < 0)
		return second_;
	
	return first_;
}

void Pair<const char*>::swap()
{
	const char* tmp = first_;
	first_ = second_;
	second_ = tmp;
}


std::ostream& operator <<(std::ostream& out, const Pair<const char*>& p)
{
	out << "[" << p.first() << ", " << p.second() << "]";
	
	return out;
}

#endif /*PAIR2_HPP_*/
