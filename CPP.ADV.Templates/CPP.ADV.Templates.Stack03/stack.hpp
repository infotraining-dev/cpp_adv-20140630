#ifndef STACK_HPP_
#define STACK_HPP_

#include <vector>
#include <stdexcept>

template <typename T, typename CONT = std::vector<T> >
class Stack
{
private:
	CONT elems_;
public:
	Stack();
	void push(const T& elem);
	void pop();
	T top() const;
};

template <typename T, typename CONT>
Stack<T, CONT>::Stack()
{
}

template <typename T, typename CONT>
void Stack<T, CONT>::push(const T& elem)
{
	elems_.push_back(elem);
}

template <typename T, typename CONT>
void Stack<T, CONT>::pop()
{
	if (elems_.empty())
		throw std::out_of_range("Stack<>. Stos pusty");
	
	elems_.pop_back();
}

template <typename T, typename CONT>
T Stack<T, CONT>::top() const
{
	if (elems_.empty())
		throw std::out_of_range("Stack<>. Stos pusty");
	
	return elems_.back();
}



#endif /*STACK_HPP_*/
