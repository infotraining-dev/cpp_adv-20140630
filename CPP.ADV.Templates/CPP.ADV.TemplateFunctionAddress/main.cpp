#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;

template <typename T>
void f(T* ptr)
{
	cout << "funkcja szablonowa f(T*)" << endl;
}

void h(void (*pf)(int*))
{
	cout << "h( void (*pf)(int*))" << endl;
}

template <typename T>
void g(void (*pf)(T*))
{
	cout << "g( void (*pf)(T*))" << endl;
}

int main()
{
	double pi = 3.14;
	f(&pi);
	
	h(&f<int>);  // pelna specyfikacja typu
	
	
	h(&f);  // odgadywanie typu
	
	
	g(&f<int>);  // pelna specyfikacja typu
	
	
	g<int>(&f);  // specyfkacja częściowa, ale wystarczająca
}
