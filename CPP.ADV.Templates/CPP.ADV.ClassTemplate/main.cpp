#include <iostream>
#include <cstdlib>
#include <string>
#include "pair.hpp"

using namespace std;

int main()
{
	Pair<int> intPair(4, 7);
	
	cout << intPair << endl;
	
	intPair.swap();
	
	cout << intPair << endl;
	
	Pair<string> stringPair(string("ALA"), string("ala"));
	
	cout << stringPair << endl;
	
	cout << "stringPair.getMax() = " << stringPair.max() << endl;		
}
