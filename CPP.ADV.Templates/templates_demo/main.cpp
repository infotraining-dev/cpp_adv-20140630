#include <iostream>
#include <cstring>
#include <memory>
#include <vector>
#include <list>

using namespace std;

template <typename T>
const T& maximum(const T& a, const T& b)
{
    cout << "maximum<T>(" << a << ", " << b << ")" << endl;
    return (a < b) ? b : a;
}

template <typename T>
shared_ptr<T> maximum(shared_ptr<T> a, shared_ptr<T> b)
{
    cout << "maximum<shared_ptr<T>>" << endl;
    return (*a < *b) ? b : a;
}

template <typename T>
const T* maximum(const T* a, const T* b)
{
    return (*a < *b) ? b : a;
}

const char* maximum(const char* txt1, const char* txt2)
{
    cout << "maximum(const_char*)" << endl;
    return strcmp(txt1, txt2) > 0 ? txt1 : txt2;
}

//template <typename T1, typename T2>
//const T1& maximum(const T1& a, const T2& b)
//{
//    cout << "maximum<T1, T2>(" << a << ", " << b << ")" << endl;
//    return (a < b) ? b : a;
//}

template <typename Container>
void print(const Container& c, const std::string& prefix)
{
    std::cout << prefix << ": [ ";
    for(typename Container::const_iterator it = c.begin(); it != c.end(); ++it)
        std::cout << *it << " ";
    std::cout << "]\n";
}

int main()
{
    int x = 10;
    int y = 14;

    cout << maximum(x, y) << endl;

    string str1 = "hello";
    string str2 = "text";

    cout << maximum(str1, str2) << endl;

    double pi = 3.1415;

    cout << maximum(static_cast<double>(x), pi) << endl;
    cout << maximum<double>(x, pi) << endl;
    //cout << maximum(x, pi) << endl;

    const char* txt1 = "Ola";
    const char* txt2 = "Ala";

    cout << maximum(txt1, txt2) << endl;

    cout << "\nshared_ptrs:" << endl;

    shared_ptr<int> sptr1(new int(30));
    shared_ptr<int> sptr2(new int(20));

    cout << *maximum(sptr1, sptr2) << endl;

    vector<int> vec1 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    print(vec1, "vec1");

    list<int> lst1 = { 34, 5, 6, 34, 65, 22, 44, 55 };

    print(lst1, "lst1");
}

