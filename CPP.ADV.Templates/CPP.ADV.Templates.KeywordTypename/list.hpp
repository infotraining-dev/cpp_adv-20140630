#ifndef LIST_HPP_
#define LIST_HPP_

#include <iostream>

template <typename T>
class List
{
private:
	typedef T* ElemT;
	
	struct Node 
	{
		ElemT ptrElem;
		Node* next;
			
		Node() : ptrElem(NULL), next(NULL)
		{
		}
	};
	
	Node* first_;
	Node* last_;

public:
	List() // -- konstruktor
	{
		first_ = last_  = NULL;
	}
	
	~List(); // -- destruktor
	
	void push_back(T& elem);
	
	void push_front(T& elem);
	
	template <typename T2> friend std::ostream& operator<<(std::ostream& out, List<T2>& lst);
};

template <typename T>
void List<T>::push_back(T& elem)
{
	Node* newNode = new Node();
	
	newNode->ptrElem = &elem;
	
	// wstawienie węzła na koniec
	if (!first_)
		first_ = newNode;
	else
		last_->next = newNode;
	
	last_ = newNode;
}

template <typename T>
void List<T>::push_front(T& elem)
{
	Node* newNode = new Node();
		
	newNode->ptrElem = &elem;
	
	newNode->next = first_;
	first_ = newNode;
}

template <typename T>
List<T>::~List()
{
	for (Node* current_ = first_; current_; )
	{
		Node* tmp; // zanim usuniety zostanie dany wezel konieczne jest zapamietanie gdzie znajduje sie jego nastepca
		
		tmp = current_->next;
		
		delete current_;
		
		current_ = tmp;
	}
}

template <typename T>
std::ostream& operator<<(std::ostream& out, List<T>& lst)
{
	typename List<T>::Node* currentNode = lst.first_; // -- typename wskazuje kompilatorowi, ze Node jest typem zdefiniowanym wewnątrz szablonu List
	
	out << "[ ";
	
	while (currentNode)
	{
		out << *(currentNode->ptrElem) << " ";
		currentNode = currentNode->next;
	}
	
	out << "]";
	
	return out;
}

#endif /*LIST_HPP_*/
