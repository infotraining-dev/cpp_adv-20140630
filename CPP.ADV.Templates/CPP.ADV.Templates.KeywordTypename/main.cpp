#include <iostream>
#include "list.hpp"

using namespace std;

int main()
{
	List<int> lstInt;
	
	int a = 9;
	int b = 10;
	int c = 1;
	
	lstInt.push_back(a);
	lstInt.push_back(b);
	lstInt.push_front(c);
	
	cout << lstInt << endl;

	return 0;
}
