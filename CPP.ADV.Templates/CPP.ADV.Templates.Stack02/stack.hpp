#ifndef STACK_HPP_
#define STACK_HPP_

#include <stdexcept>

template <typename T, size_t N = 10>
class Stack
{
public:
	Stack();
	void push(const T& elem);  // dodaje element na szczycie stosu
	void pop();					 // usuwa element ze szczytu stosu
	T top() const;              // zwraca element znajduj�cy si� na szczycie 
	
private:
	size_t size_;
	T elems_[N];
};

template <typename T, size_t N>
Stack<T, N>::Stack() : size_(0)
{
}

template <typename T, size_t N>
void Stack<T, N>::push(const T& elem)
{
	if (size_ == N)
		throw std::out_of_range("Przepe�nienie stosu");
	
	elems_[size_++] = elem;
}

template <typename T, size_t N>
void Stack<T, N>::pop() 
{
	if (size_ == 0)
		throw std::out_of_range("Stos pusty");
	
	size_--;
}

template <typename T, size_t N>
T Stack<T, N>::top() const
{
	if (size_ == 0)
		throw std::out_of_range("Stos pusty");
	return elems_[size_ - 1];
}

#endif /*STACK_HPP_*/
