#include <iostream>
#include "stack.hpp"

using namespace std;

int main()
{
	int numbers[5] = {1, 2, 3, 4, 5};
	
	try
	{
        Stack<int, 10> stack10Int;

        Stack<int, 10> other_stack;
		
		stack10Int.push(6);
		
		cout << stack10Int.top() << endl;
		
		for (int i = 0; i < 5; ++i) {
			stack10Int.push(numbers[i]);
		}

		for (int i = 0; i < 10; ++i)
		{
			cout << "Bierzący element na stosie: " << stack10Int.top() << endl;
			stack10Int.pop();
		}
	}
	catch (const std::exception& e)
	{
		cout << e.what() << endl;
	}
}
