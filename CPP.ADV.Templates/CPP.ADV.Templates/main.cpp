#include <iostream>
#include <string>
#include <cstdlib>
#include <string.h> // strcmp in gcc at least...

template <typename T> 
const T& max(const T& a, const T& b)
{
	return a < b ? b : a;
}

const char* max(const char* a, const char* b)
{
	if (strcmp(a, b) < 0)
		return b;

	return a;
}

template<typename T>
void my_swap(T& a, T& b)
{
	T tmp(a);
	a = b;
	b = tmp;
}

template<> void my_swap(std::string& a, std::string& b)
{
	std::string tmp(a);
	a = b;
	b = tmp;
}

void my_swap(std::string& s1, std::string& s2)
{
	s1.swap(s2);
}


int main()
{
	using std::cout;
	using std::endl;
	using std::string;

	cout << "max(5, 6) = " <<:: max(5, 6) << endl; // prints

	string str1 = "Ala";
	string str2 = "ala";

	cout << "max(string(\"Ala\"), string(\"ala\")) = " << ::max(str1, str2) << endl;

	const char* txt1 = "ala";
	const char* txt2 = "ALA";

	cout << "max(const char*, const char*) = " << ::max(txt1, txt2) << endl;

 	cout << "txt1 = " << txt1 << "; txt2 = " << txt2 << endl;

	my_swap(txt1, txt2);

	cout << "po swap(txt1, txt2) : txt1 = " << txt1 << " - txt2 = " << txt2 << endl;

	cout << "str1 = " << str1 << "; str2 = " << str2 << endl;

	my_swap<>(str1, str2);

	cout << "po swap(str1, str2) str1 = " << str1 << "; str2 = " << str2 << endl;
}
