#include <iostream>
#include <cassert>

using namespace std;

template <typename T>
class my_auto_ptr
{
private:
    T* ptr_;
public:
    my_auto_ptr(T* ptr) : ptr_(ptr)
    {}

    my_auto_ptr(my_auto_ptr& source)
    {
        ptr_ = source.ptr_;
        source.ptr_ = NULL;
    }

    my_auto_ptr& operator=(my_auto_ptr& source)
    {
        ptr_ = source.ptr_;
        source.ptr_ = NULL;

        return *this;
    }

    void swap(my_auto_ptr& other)
    {
        std::swap(ptr_, other.ptr_);
    }

    T& operator*() const
    {
        return *ptr_;
    }

    T* operator->() const
    {
        return ptr_;
    }

    ~my_auto_ptr()
    {
        delete ptr_;
    }

    T* get() const
    {
        return ptr_;
    }
};

void foo(int* arg)
{
    cout << "foo(" << *arg << ")" << endl;
}

int main()
{
    my_auto_ptr<int> ptr1(new int(20));

    cout << "*ptr1 = " << *ptr1 << endl;

    foo(ptr1.get());

    my_auto_ptr<int> ptr2 = ptr1;

    assert(ptr1.get() == NULL);

    foo(ptr2.get());
}

