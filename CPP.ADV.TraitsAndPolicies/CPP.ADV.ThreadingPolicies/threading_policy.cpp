/*
 * threading_policy.cpp
 *
 *  Created on: 2010-09-07
 *      Author: InfoTraining.KP
 */

#include <iostream>
#include <boost/thread.hpp>

class SimpleMutex
{
public:
	void lock()
	{
		std::cout << "Locking simple mutex." << std::endl;
	}

	void unlock()
	{
		std::cout << "Unlocking simple mutex" << std::endl;
	}
};

class SingleThreadedPolicy
{
protected:
	class Mutex {};
	Mutex mtx_;

	class Lock
	{
	public:
		Lock(Mutex& ) {}
		~Lock() {}
	};
};

class MultithreadedPolicy
{
protected:
	typedef SimpleMutex Mutex;
	Mutex mtx_;

	class Lock
	{
		Mutex& mtx_;
	public:
		explicit Lock(Mutex& mtx) : mtx_(mtx)
		{
			mtx_.lock();
		}

		~Lock()
		{
			mtx_.unlock();
		}
	};
};

class BoostThreadedPolicy
{
protected:
	boost::mutex mtx_;

	typedef boost::lock_guard<boost::mutex> Lock;
};

template <typename ThreadingPolicy = BoostThreadedPolicy>
class TestThreading : protected ThreadingPolicy
{
	typedef typename ThreadingPolicy::Lock LockGuard;
public:
	void test()
	{
		LockGuard lk(ThreadingPolicy::mtx_);
		std::cout << "test\n";
	}
};


int main()
{
    TestThreading<MultithreadedPolicy> t;

	t.test();
}
