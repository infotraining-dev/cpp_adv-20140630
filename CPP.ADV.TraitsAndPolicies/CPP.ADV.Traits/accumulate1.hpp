/*
 * accumulate.hpp
 *
 *  Created on: 2010-03-13
 *      Author: InfoTraining.KP
 */

#ifndef ACCUMULATE_HPP_
#define ACCUMULATE_HPP_

template <typename T>
T accum(const T* begin, const T* end)
{
	T total = T();
	while(begin != end)
	{
		total += *begin;
		++begin;
	}

	return total;
}

#endif /* ACCUMULATE_HPP_ */
