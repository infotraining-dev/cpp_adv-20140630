/*
 * main.cpp
 *
 *  Created on: 2010-03-13
 *      Author: InfoTraining.KP
 */

#include <iostream>
//#include "accumulate1.hpp"

//#include "accumulate2.hpp"

#include "accumulate3.hpp"

using namespace std;

template <>
class AccumulationTraits<string>
{
public:
    typedef string AccT;
    static AccT zero() {
        return "";
    }
};

int main()
{
	int numbers[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    cout << "sum of the integer values: "
         << accum(numbers, numbers+10) << "\n\n";

    char word[] = "templates";
    int length = sizeof(word) - 1;

    std::cout << "average value of characters in " << word << ": "
            << accum(word, word+length)/length << "\n\n";

    string words[] = { "one", "two", "three" };

    std::cout << accum(words, words+3) << std::endl;

	// obliczenie iloczynu liczb
    std::cout << "product of numbers: "
            << accum<int, MultPolicy>(numbers, numbers+10) << "\n";

}
