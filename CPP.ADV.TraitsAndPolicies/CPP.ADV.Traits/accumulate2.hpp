/*
 * accumulate.hpp
 *
 *  Created on: 2010-03-13
 *      Author: InfoTraining.KP
 */

#ifndef ACCUMULATE_HPP_
#define ACCUMULATE_HPP_

template <typename T>
class AccumulationTraits
{
public:
    typedef T AccT;
    static T zero() { return T(); }
};

template <>
class AccumulationTraits<int>
{
public:
	typedef long AccT;
    static long zero() { return 0L; }
};

template <>
class AccumulationTraits<char>
{
public:
	typedef int AccT;
    static int zero() { return 0; }
};

template <typename T>
typename AccumulationTraits<T>::AccT accum(const T* begin, const T* end)
{
	typedef typename AccumulationTraits<T>::AccT AccT;
    AccT total = AccumulationTraits<T>::zero();
	while(begin != end)
	{
		total += *begin;
		++begin;
	}

	return total;
}

#endif /* ACCUMULATE_HPP_ */
