/*
 * accumulate.hpp
 *
 *  Created on: 2010-03-13
 *      Author: InfoTraining.KP
 */

#ifndef ACCUMULATE_HPP_
#define ACCUMULATE_HPP_

// klasy cech
template<typename T>
class AccumulationTraits;

template<>
class AccumulationTraits<char> {
  public:
    typedef int AccT;
    static AccT zero() {
        return 0;
    }
};

template<>
class AccumulationTraits<short> {
  public:
    typedef int AccT;
    static AccT zero() {
        return 0;
    }
};

template<>
class AccumulationTraits<int> {
  public:
    typedef long AccT;
    static AccT zero() {
        return 0;
    }
};

// klasy wytycznych
class SumPolicy {
  public:
    template<typename T1, typename T2>
    static void accumulate (T1& total, T2 const & value) {
        total += value;
    }
};

class MultPolicy {
  public:
    template<typename T1, typename T2>
    static void accumulate (T1& total, T2 const& value) {
        total *= value;
    }
};


// algorytm kumulacji
template
<
	typename T,
    typename Policy = SumPolicy,
    typename Traits = AccumulationTraits<T>
>
class Accum {
  public:
    static typename Traits::AccT accum (T const* beg, T const* end) {
        typename Traits::AccT total = Traits::zero();
        while (beg != end) {
            Policy::accumulate(total, *beg);
            ++beg;
        }
        return total;
    }
};


// funkcje upraszczające interfejs
template <typename T>
inline
typename AccumulationTraits<T>::AccT accum (T const* beg,
                                            T const* end)
{
    return Accum<T>::accum(beg, end);
}

template <typename T, typename Policy>
inline
typename AccumulationTraits<T>::AccT accum (T const* beg,
                                            T const* end)
{
    return Accum<T, Policy>::accum(beg, end);
}

template <typename T, typename Policy, typename Traits>
inline
typename Traits::AccT accum (T const* beg, T const* end)
{
    return Accum<T, Policy, Traits>::accum(beg, end);
}

#endif /* ACCUMULATE_HPP_ */
