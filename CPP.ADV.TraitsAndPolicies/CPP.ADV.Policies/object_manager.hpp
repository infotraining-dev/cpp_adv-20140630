#ifndef OBJECTMANAGER_HPP_
#define OBJECTMANAGER_HPP_

#include <map>
#include <cstdlib>

// klasa wytycznej - tworzenie obiektu poprzez użycie operatora new
template<typename T>
class OperatorNewCreator
{
public:
	static T* create()
	{
		return new T;
	}

	static void destroy(T* ptr_obj)
	{
		delete ptr_obj;
	}
};

// klasa wytycznej - tworzenie obiektu poprzez użycie operatora new
template<typename T>
class MallocCreator
{
public:
	static T* create()
	{
		void* buffer = std::malloc(sizeof(T));
		if (!buffer)
			return 0;
		return new (buffer) T;
	}

	static void destroy(T* ptr_obj)
	{
		ptr_obj->~T();
		std::free(ptr_obj);
	}
};

// klasa wytycznej - tworzenie obiektu poprzez użycie prototypowego egzemplarza
template<typename T>
class PrototypeCreator
{
private:
	T* prototype_;
public:
	PrototypeCreator(T* ptr_obj = NULL) : prototype_(ptr_obj)
	{}

	T* create()
	{
		return prototype_ ? prototype_->clone() : 0;
	}

	void destroy(T* ptr_obj)
	{
		delete ptr_obj;
	}

	T* get_prototype()
	{
		return prototype_;
	}

	void set_prototype(T* ptr_obj)
	{
		prototype_ = ptr_obj;
	}
};

template
<
	typename T,
	template<typename T> class CreationPolicy
>
class ObjectManager :  public CreationPolicy<T>
{
private:
	std::map<std::string, T*> objects_;
public:
	T* create_object(const std::string& id)
	{
		T* ptr_obj = CreationPolicy<T>::create();
		objects_[id] = ptr_obj;
		return ptr_obj;
	}

	T* operator[](const std::string& id)
	{
		return objects_[id];
	}


	~ObjectManager()
	{
		typename std::map<std::string, T*>::const_iterator it = objects_.begin();
		for(; it != objects_.end(); ++it)
			CreationPolicy<T>::destroy(it->second);
	}
};

#endif /* OBJECTMANAGER_HPP_ */
