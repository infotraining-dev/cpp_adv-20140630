#include <iostream>
#include "object_manager.hpp"


class Widget
{
	std::string title_;
public:
	Widget()
	{
		std::cout << "Widget()" << std::endl;
	}

	~Widget()
	{
		std::cout << "~Widget(title=" << title_ << ")" << std::endl;
	}

	void set_title(const std::string& title)
	{
		title_ = title;
	}

	void show()
	{
		std::cout << "Showing " << title_ << " widget." << std::endl;
	}

	Widget* clone()
	{
		std::cout << "Cloning " << title_ << " widget" << std::endl;
		return new Widget(*this);
	}
};

int main()
{

	std::cout << "Using operator new policy...\n";
	{
		ObjectManager<Widget, OperatorNewCreator> widget_manager;

		Widget* w = widget_manager.create_object("button1");
		w->set_title("OK");

		w = widget_manager.create_object("button2");
		w->set_title("Cancel");
		widget_manager["button1"]->show();
	}

	std::cout << "\nUsing malloc policy...\n";
	{
		ObjectManager<Widget, MallocCreator> widget_manager;

		Widget* w = widget_manager.create_object("button1");
		w->set_title("OK");

		w = widget_manager.create_object("button2");
		w->set_title("Cancel");

		widget_manager["button2"]->show();
	}

	std::cout << "\nUsing prototyping policy...\n";
	{
		ObjectManager<Widget, PrototypeCreator> widget_manager;

		// ustawienie prototypowego egzemplarza
		Widget* prototype = new Widget();
		prototype->set_title("not set");
		widget_manager.set_prototype(prototype);

		Widget* w = widget_manager.create_object("button1");
		w->set_title("OK");

		w = widget_manager.create_object("button2");
		w->set_title("Cancel");

		widget_manager["button2"]->show();

		// usunięcie prototypu
		delete widget_manager.get_prototype();
	}
}
