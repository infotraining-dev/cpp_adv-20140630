/*
 * traits_exercise.cpp
 *
 *      Author: InfoTraining.KP
 */

#include <iostream>

#include <deque>

// wytyczna PtrDeletePolicy - odpowiedzialana za usunięcie wskaźników
// TODO:

// wytyczna ArrayDeletePolicy - odpowiedzialna za usunięcie tablic
// TODO:

// wytyczne NoDeletePolicy - nic nie usuwa
// TODO:

template <
	typename T
	// TODO: wytyczna - DeletionPolicy
>
class Stack
{
public:
	typedef std::deque<T> ContainerType;
	typedef typename std::deque<T>::iterator IteratorType;

public:
	Stack()
	{
	}

	~Stack()
	{
		// TODO: Implementacja destruktora
	}

	void push(const T& elem)
	{
		// TODO: Implementacja
	}

	void pop(T& element)
	{
		// TODO: Implementacja
	}
private:
	ContainerType stack_impl_;
};

class Object
{
	static int gen_id_;
	int id_;
public:
	Object() : id_(++gen_id_)
	{
		std::cout << "Object(" << id_ << ")" << std::endl;
	}

	Object(const Object& o) : id_(o.id_)
	{
		std::cout << "Object(" << id_ << ")" << std::endl;
	}

	~Object()
	{
		std::cout << "~Object(" << id_ << ")" << std::endl;
	}
};

int Object::gen_id_ = 0;

int main()
{
//	// zwykle obiekty na stosie
//	std::cout << "-------------------\n";
//	{
//		Stack<Object> s1;
//
//		s1.push(Object());
//		s1.push(Object());
//	}
//
//	// wskaźniki na stosie
//	std::cout << "\n\n-------------------\n";
//	{
//		Stack<Object*, PtrDeletePolicy> s1;
//
//		s1.push(new Object());
//		s1.push(new Object());
//	}
//
//	// tablice na stosie
//	std::cout << "\n\n-------------------\n";
//	{
//		Stack<Object*, ArrayDeletePolicy> s1;
//		s1.push(new Object[4]);
//		s1.push(new Object[2]);
//	}
}
