#include <iostream>
#include <iterator>
#include <cstdlib>
#include <stdexcept> 
#include <string.h> 

using namespace std;

class Message 
{

public:
    Message(int bufSize = DEFAULT_BUF_SIZE) :
        bufSize_(bufSize),
        initBufSize_(bufSize),
        msgSize_(0),
        buf_(NULL)
    {
        buf_ = new char[bufSize];
    }

    ~Message( )
    {
        delete[] buf_;
    }

    // dołączanie znaków
    void appendData(const char* data)
    {
        const int len = strlen(data);

        if (msgSize_+len > MAX_SIZE) {
            throw out_of_range("Rozmiar danych przekracza limit.");
        }

        if (msgSize_+len > bufSize_) {
            int newBufSize = bufSize_;
            while ((newBufSize *= 2) < msgSize_+len);

            char* temp_buffer = new char[newBufSize];    // przydział pamięci dla nowego bufora

            copy(buf_, buf_+msgSize_, temp_buffer);      // kopiowanie poprzedniej zawartości
            copy(data, data+len, temp_buffer+msgSize_);  // kopiowanie nowych danych

            msgSize_ += len;
            bufSize_ = newBufSize;

            delete[] buf_;  // pozbycie się poprzedniego bufora i
            buf_ = temp_buffer;       // przestawienie wskaźnika bufora na nowy
        }
        else
        {
            copy(data, data+len, buf_+msgSize_);
            msgSize_ += len;
        }
    }

    // kopiowanie danych do bufora wskazanego przez wywołującego
    int getData(int maxLen, char* data)
    {
        if (maxLen < msgSize_) {
            throw out_of_range("Bufor docelowy nie pomieści danych.");
        }

        copy(buf_, buf_+msgSize_, data);
        return(msgSize_);
    }

    Message(const Message& org); // TO DO
    Message& operator=(const Message& rhs); // TO DO

private:
    int bufSize_;
    int initBufSize_;
    int msgSize_;
    char* buf_;

    void bufferToStream(std::ostream& out) const
    {
        copy(buf_, buf_+msgSize_,
             std::ostream_iterator<char>(out, ""));
    }

    static const int DEFAULT_BUF_SIZE = 10;
    static const int MAX_SIZE = 24;

    friend std::ostream& operator<<(std::ostream& out, const Message& msg)
    {
        msg.bufferToStream(out);

        return out;
    }
};

int main()
{
    Message msg(5);

    try
    {
        msg.appendData("Msg;");

        std::cout << msg << endl;

        msg.appendData("Test klasy Message;");

        std::cout << msg << endl;

        msg.appendData("Nowy klasy Message;");
    }
    catch(const std::out_of_range& e)
    {
        std::cout << e.what() << std::endl;
    }

    std::cout << msg << endl;
}	
