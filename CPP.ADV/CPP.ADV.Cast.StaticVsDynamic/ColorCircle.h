//---------------------------------------------------------------------------

#ifndef ColorCircleH
#define ColorCircleH
#include "Circle.h"

namespace Drawing
{

//---------------------------------------------------------------------------
class ColorCircle : public Circle
{
private:
    int color_;

public:
    ColorCircle(const Coord& pos = Coord(0,0), int r = 0, int color = 0);

	ColorCircle(int x, int y, int r=0, int color = 0);

	~ColorCircle() {};

	void draw() const;
};

}

#endif
