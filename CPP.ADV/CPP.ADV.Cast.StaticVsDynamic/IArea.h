//---------------------------------------------------------------------------

#ifndef IAreaH
#define IAreaH
//---------------------------------------------------------------------------

// Basic Abstract Class
class IArea
{
public:
	virtual double area() const = 0;
	virtual ~IArea() {}
};

#endif
