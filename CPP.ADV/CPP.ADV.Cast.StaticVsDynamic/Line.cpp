//---------------------------------------------------------------------------

#include "Line.h"

using namespace std;

namespace Drawing
{
	//---------------------------------------------------------------------------
	Line::Line(const Coord& pos, const Coord& end)
		: Shape(pos), end_point_(end) {}

	//---------------------------------------------------------------------------
	Line::~Line() 
	{
	}

	//---------------------------------------------------------------------------
	void Line::move_to(const Coord& new_pos)
	{
		int dx = new_pos.x - position().x;
		int dy = new_pos.y - position().y;

		Shape::move_to(new_pos);
		end_point_.x += dx;
		end_point_.y += dy;
	}


	//---------------------------------------------------------------------------
	void Line::draw() const
	{
        cout << "Drawing Line: " << position_ << "  " << end_point_ << "\n";
	};

}


