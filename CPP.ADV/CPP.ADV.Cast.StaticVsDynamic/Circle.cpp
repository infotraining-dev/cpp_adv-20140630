//---------------------------------------------------------------------------
#include "Circle.h"

using namespace std;

namespace Drawing
{
	//---------------------------------------------------------------------------
	Circle::Circle(const Coord& pos, int r) 
		: Shape(pos), radius_(r) 
	{
	}

	//---------------------------------------------------------------------------
	Circle::Circle(int x, int y, int r) 
	{
		position_.x = x;
		position_.y = y;
		radius_ = r;
	}

	//---------------------------------------------------------------------------
	Circle::~Circle()
	{
	}

	//---------------------------------------------------------------------------
	void Circle::draw() const
	{
        cout << "Drawing Circle: [" << position_.x << ", " << position_.y << "]\n";
	};

	//---------------------------------------------------------------------------
	void Circle::roll() const
	{
        cout << "Rolling Circle: [" << position_.x << ", " << position_.y << "]\n";
	};
	
	//---------------------------------------------------------------------------
	double Circle::area() const
	{
		return M_PI * radius_ * radius_;
	}
}
