//---------------------------------------------------------------------------
#include "ColorCircle.h"


namespace Drawing
{
	//---------------------------------------------------------------------------
	ColorCircle::ColorCircle(const Coord& pos, int r, int color) 
		: Circle(pos, r), color_(color) 
	{
	}

	//---------------------------------------------------------------------------
	ColorCircle::ColorCircle(int x, int y, int r, int color) 
	{
		position_.x = x;
		position_.y = y;
		radius_ = r;
		color_ = color;
	}

	//---------------------------------------------------------------------------
	void ColorCircle::draw() const
	{
		using namespace std;
        cout << "Drawing ColorCircle: [" << position_.x << ", " << position_.y << "] Color:" << color_ << "\n";
	};

}


