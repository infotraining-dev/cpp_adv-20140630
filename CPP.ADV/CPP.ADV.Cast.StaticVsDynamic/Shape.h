//---------------------------------------------------------------------------

#ifndef ShapeH
#define ShapeH
#include <iostream>
#include <cstdlib>

namespace Drawing
{

	//---------------------------------------------------------------------------
	struct Coord 
	{
		int x, y;

		Coord(int x=0, int y=0) : x(x), y(y) {};
	};

	std::ostream& operator<<(std::ostream& out, const Coord& coord);

	// klasa abstrakcyjna
	class Shape
	{
	protected:
		Coord position_;

	public:
		Shape(int x = 0, int y = 0);
		Shape(const Coord& pos);
		virtual ~Shape();

		Coord position() const;
		virtual void move_to(const Coord& new_pos);
		virtual void draw() const = 0;  //metoda czysto wirtualna
	};

	inline Coord Shape::position() const
	{
		return position_;
	};

}

//---------------------------------------------------------------------------
#endif
