//---------------------------------------------------------------------------

#ifndef LineH
#define LineH

#include "Shape.h"
//---------------------------------------------------------------------------

namespace Drawing
{

	class Line : public Shape
	{
	protected:
		Coord end_point_;

	public:
		Line(const Coord& pos = Coord(0,0), const Coord& end = Coord(0,0));
		~Line();

		void draw() const;
		void move_to(const Coord& newPos);

		Coord end_point() const
		{
			return end_point_;
		}

		void set_end_point(const Coord& end_point)
		{
			end_point_ = end_point;
		}
	};
}
#endif
