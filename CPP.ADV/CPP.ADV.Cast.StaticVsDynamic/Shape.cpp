//---------------------------------------------------------------------------

#include "Shape.h"

//---------------------------------------------------------------------------
Drawing::Shape::Shape(int x, int y) 
{
	position_.x = x;
    position_.y = y;
};

Drawing::Shape::~Shape()
{
}

namespace Drawing
{

	//---------------------------------------------------------------------------
	Shape::Shape(const Coord& pos) : position_(pos) 
	{
    }

	
	//---------------------------------------------------------------------------
	void Shape::move_to(const Coord& new_pos)
	{
		position_ = new_pos;
	}


	//---------------------------------------------------------------------------
	std::ostream& operator<<(std::ostream& out, const Coord& coord)
	{
		out << "[" << coord.x << ", " << coord.y << "]";

		return out;
	}
}