//---------------------------------------------------------------------------

#ifndef CircleH
#define CircleH

#define _USE_MATH_DEFINES
#include <cmath>

#include "Shape.h"
#include "IArea.h"

//---------------------------------------------------------------------------

namespace Drawing
{

	class Circle : public Shape, public IArea
	{
	protected:
		int radius_;

	public:
		Circle(const Coord& pos = Coord(0, 0), int r = 0);
		Circle(int x, int y, int r=0);
		~Circle();

		virtual void draw() const;

		virtual double area() const;

		void roll() const;

		int radius() const;
		void set_radius(int r);
	};


	inline int Circle::radius() const
	{
		return radius_;
	}

	inline void Circle::set_radius(int r)
	{
		radius_ = r;
	}
}


//---------------------------------------------------------------------------
#endif
