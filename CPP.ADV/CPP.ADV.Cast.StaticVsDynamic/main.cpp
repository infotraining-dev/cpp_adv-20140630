//---------------------------------------------------------------------------

#include <string>
#include "ColorCircle.h"
#include <stdexcept>
#include "Line.h"
#include "IArea.h"

using namespace Drawing;

Shape* create_shape(const std::string& id)
{
	if (id == "Circle")
		return new Circle();
	else if (id  == "ColorCircle")
		return new ColorCircle();
	else if (id == "Line")
		return new Line();

    throw std::runtime_error("Invalid shape ID");
}

void draw_shapes(Shape* shapes[], int size)
{
	for (int i = 0; i < size; i++) {
		shapes[i]->draw();
	}
};

double calculate_total_area(Shape* shapes[], int size)
{
	double total = 0.0;

	for (int i = 0; i < size; i++) {
	
		// TODO
		// bezpieczne rzutowanie z Shape* na IArea*
	}

	return total;
}

double calculate_area(Shape& shape)
{
	double area = 0.0;
	
	// TODO - bezpieczne rzutowanie Shape& na IArea&

	return area;
}


//---------------------------------------------------------------------------
int main()
{
	Circle circle1(Coord(0, 0), 1);
	circle1.move_to(Coord(5,15));
	circle1.draw();

	Line line1(Coord(0, 0), Coord(3, 3));
	line1.move_to(Coord(10, 10));
	line1.draw();

	// rzutowanie w hierarchii dziedziczenia
	const Shape* shape = &circle1;
	Circle* pCircle = NULL;
	
    // rzutowanie Shape* -> Circle* - bezpieczne, bo wiem jaki jest typ wskazywanego obiektu
	// TODO
	
	std::cout << "----------------------\n";

	Shape* geoShapes[] = { &circle1, &line1, new Line(Coord(5, 5), (6, 10)), new ColorCircle(1, 1, 3, 255) };
	
	draw_shapes(geoShapes, 4);

	std::cout << "----------------------\n";

	// obliczenie powierzchni ksztaltow
	std::cout << "Total Area: " << calculate_total_area(geoShapes, 4) << std::endl;

	std::cout << "----------------------\n";

	// obliczenie powierzchni ksztaltow
	std::cout << "Area - circle1: " << calculate_area(circle1) << std::endl;
	std::cout << "Area - line1: " << calculate_area(line1) << std::endl;
	
	delete geoShapes[2];
	delete geoShapes[3];
}
//---------------------------------------------------------------------------
