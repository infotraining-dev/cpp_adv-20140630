#include <iostream>
#include <stdexcept>
#include <exception>
#include <typeinfo>

using namespace std;

class Base
{
public:
    virtual void do_sth() const
    {
        cout << "Base::do_sth()" << endl;
    }

    virtual ~Base() {}
};

class Derived : public Base
{
public:
    virtual void do_sth() const //override
    {
        cout << "Derived::do_sth()" << endl;
    }

    virtual void extra()
    {
        cout << "Derived::extra()" << endl;
    }
};

void foo(Base& b)
{
    b.do_sth();

    try
    {
        Derived& d = dynamic_cast<Derived&>(b);

        d.extra();
    }
    catch(const bad_cast& e)
    {
        cout << "Exception: " << e.what() << endl;
    }
}

int main()
{
    const Base* ptr_base = new Derived();

    ptr_base->do_sth();

    Derived* ptr_derived =
            const_cast<Derived*>(
                dynamic_cast<const Derived*>(ptr_base));

    if (ptr_derived)
        ptr_derived->extra();
    else
        cout << "Nieudana konwersja" << endl;

    delete ptr_base;

    Derived d;
    Base b;

    foo(d);
    foo(b);
}

