#include <iostream>
#include <stdexcept>
#include <cstdlib>

using namespace std;

void my_terminate_handler()
{
    cout << "Blad! Zadzwon na 0-700-788-788..." << endl;

    exit(1);
}

class Destrucible
{
    int val_;
public:
    Destrucible(int val) : val_(val)
    {
        cout << "Destrucible(" << val_ << ")" << endl;
    }

    ~Destrucible()
    {
        cout << "~Destrucible(" << val_ << ")" << endl;
    }
};

int divide(int n, int d)
{
    Destrucible d1(1);
    if (d == 0)
        throw std::invalid_argument("Dzielnik nie moze byc zerem");

    if (n == 13)
        throw std::runtime_error("Nie lubie 13");

    return n / d;
}

int main() try
{
    set_terminate(&my_terminate_handler);

    Destrucible d0(0);

    int result = divide(13, 2);

    cout << "Result: " << result << endl;
}
catch(const invalid_argument& e)
{
    cout << "Exception: " << e.what() << endl;

    exit(1);
}
catch(...)
{
    cout << "Exception" << endl;

    throw;
}

