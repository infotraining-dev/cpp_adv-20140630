#include <iostream>
#include <chrono>
#include <boost/function.hpp>
#include <boost/pool/object_pool.hpp>
#include "memory_pool.hpp"

using namespace std;
using namespace boost::chrono;

const size_t N = 1000000;
const size_t TAB_SIZE = 10;

struct Data
{
	int x;
	double d;
};

struct DataFromPool : public Pool<Data, 5>
{
	int x;
	double d;
};

void test_efficiency(boost::function<void()> f, const string& description)
{
	auto t1 = system_clock::now();

	f();

	auto t2 = system_clock::now();

	nanoseconds ns = t2 - t1;

	cout << description << ": " << duration_cast<milliseconds>(ns).count() << " ms" << endl;
}

void std_new_delete()
{
	Data* ptrs[TAB_SIZE];
	for(size_t i = 0; i < N; ++i)
	{
		for(size_t j = 0; j < TAB_SIZE; ++j)
		{
			ptrs[j] = new Data;
		}

		for(size_t j = 0; j < TAB_SIZE; ++j)
		{
			delete ptrs[j];
		}
	}
}

void boost_object_pool()
{
    boost::object_pool<Data> pool;
	Data* ptrs[TAB_SIZE];
	for(size_t i = 0; i < N; ++i)
	{
		for(size_t j = 0; j < TAB_SIZE; ++j)
		{
			ptrs[j] = pool.malloc();
		}

		for(size_t j = 0; j < TAB_SIZE; ++j)
		{
			pool.free(ptrs[j]);
		}
	}
}

void overloaded_new_delete()
{
	DataFromPool::initialize_pool();

	DataFromPool* ptrs[TAB_SIZE];
	for(size_t i = 0; i < N; ++i)
	{
		for(size_t j = 0; j < TAB_SIZE; ++j)
		{
			ptrs[j] = new DataFromPool;
		}

		for(size_t j = 0; j < TAB_SIZE; ++j)
		{
			delete ptrs[j];
		}
	}

	DataFromPool::delete_pool();
}

int main()
{
	test_efficiency(std_new_delete, "std_new_delete"); 
	test_efficiency(std_new_delete, "std_new_delete"); 

	cout << "\n\n";

    test_efficiency(boost_object_pool, "boost_object_pool");
	test_efficiency(boost_object_pool, "boost_object_pool");
	
	cout << "\n\n";
	
	test_efficiency(overloaded_new_delete, "overloaded_new_delete"); 
	test_efficiency(overloaded_new_delete, "overloaded_new_delete"); 
}
