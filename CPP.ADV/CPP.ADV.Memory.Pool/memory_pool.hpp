#ifndef MEMORY_POOL_HPP
#define MEMORY_POOL_HPP

template <typename T, size_t EXPANSION_SIZE = 32>
class MemoryPool
{
public:
	MemoryPool();
	~MemoryPool();	
	void* alloc(size_t size);
	void free(void* element);
private:
	typedef MemoryPool<T, EXPANSION_SIZE> MemoryPoolType;
	typedef MemoryPoolType* MemoryPoolPointer;

	void expand_free_list();

	MemoryPoolPointer next;
};

template <typename T,  size_t EXPANSION_SIZE>
MemoryPool<T, EXPANSION_SIZE>::MemoryPool()
{
	expand_free_list();
}

template <typename T, size_t EXPANSION_SIZE>
MemoryPool <T, EXPANSION_SIZE>::~MemoryPool()
{
	for (MemoryPoolPointer nextPtr = next; nextPtr != NULL; nextPtr = next) 
	{
		next = next->next;
		::operator delete(nextPtr);
	}
}

template <typename T, size_t EXPANSION_SIZE>
void* MemoryPool<T, EXPANSION_SIZE>::alloc(size_t)
{
	if (!next) 
		expand_free_list();
	
	MemoryPoolPointer head = next;
	next = head->next;
	return head;
}

template <typename T, size_t EXPANSION_SIZE>
void MemoryPool<T, EXPANSION_SIZE>::free(void *element)
{
	MemoryPoolPointer head = static_cast<MemoryPoolPointer>(element);
	head->next = next;
	next = head;
}

template <typename T, size_t EXPANSION_SIZE>
void MemoryPool<T, EXPANSION_SIZE>::expand_free_list()
{
	size_t size = (sizeof(T) > sizeof(MemoryPoolPointer)) ? sizeof(T) : sizeof(MemoryPoolPointer);

	MemoryPoolPointer runner = reinterpret_cast<MemoryPoolPointer>(::operator new(size));
	next = runner;
	
	for (size_t i = 0; i < EXPANSION_SIZE-1 ; ++i) 
	{
		runner->next = reinterpret_cast<MemoryPoolPointer>(::operator new(size));
		runner = runner->next;
	}

	runner->next = 0;
}


template <typename T, size_t EXPANSION_SIZE = 32>
class Pool
{
	typedef MemoryPool<T, EXPANSION_SIZE> MemoryPoolType;
	
	static MemoryPoolType* mem_pool;
public:
	static void initialize_pool()
	{
		mem_pool = new MemoryPoolType();
	}

	static void delete_pool()
	{
		delete mem_pool;
	}

	static void* operator new(size_t size)
	{
		void* memptr = mem_pool->alloc(size);
		return memptr;
	}

	static void operator delete(void* memptr, size_t size)
	{
		mem_pool->free(memptr);
	}
};

template <typename T, size_t EXPANSION_SIZE> 
MemoryPool<T, EXPANSION_SIZE>* Pool<T, EXPANSION_SIZE>::mem_pool = 0;

#endif

