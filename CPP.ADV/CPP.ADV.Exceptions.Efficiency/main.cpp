#include <stdio.h>
#include <iostream>
#include <stdexcept>
#include <chrono>
#include <cstdlib>

using namespace std;

const size_t MAXSIZE = 100000;
const size_t FREQ = 1000;

inline int func_without_exception(int x)
{
	if ( !(x % FREQ) )
		return -1;
	
    return 0;
}

inline int func_with_exception(int x)
{
	if ( !(x % FREQ) )
		throw std::out_of_range("Exception");
	
    return 0;
}

void test_no_exception()
{ 
    auto start = std::chrono::high_resolution_clock::now();

	// your other code here

	size_t count = 0;
	int result;
	while(count < MAXSIZE)
	{
        for(size_t i = 0; i < MAXSIZE; ++i)
		{
			result = func_without_exception(i);
			if (result == -1)
				count++;
		}
	}

    auto end = std::chrono::high_resolution_clock::now();
    float elapsedSeconds = std::chrono::duration_cast<std::chrono::duration<float>>(end-start).count();
    std::cout << "Program took: " << elapsedSeconds << " seconds" << std::endl;
}

void test_exception()
{
    auto start = std::chrono::high_resolution_clock::now();

	size_t count = 0;
    int result;

    while(count < MAXSIZE)
	{
        for(size_t i = 0; i < MAXSIZE; ++i)
		{
			try
			{
				result = func_with_exception(i);
			}
			catch(const out_of_range&)
			{
				count++;
			}
		}
	}

    auto end = std::chrono::high_resolution_clock::now();
    float elapsedSeconds = std::chrono::duration_cast<std::chrono::duration<float>>(end-start).count();
    std::cout << "Program took: " << elapsedSeconds << " seconds" << std::endl;
}

int main()
{
	test_no_exception();

    std::cout << "--------------------------" << std::endl;

	test_exception();
}
