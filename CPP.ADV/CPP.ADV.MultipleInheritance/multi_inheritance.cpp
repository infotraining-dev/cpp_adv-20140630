#include <iostream>
#include <vector>

using namespace std;

class MBase 
{
public:
	virtual const char* vf() const = 0;
	virtual ~MBase() {}
};

class D1 : public MBase 
{
public:
	const char* vf() const { return "D1"; }
};

class D2 : public MBase 
{
public:
	const char* vf() const { return "D2"; }
};

// Causes error: ambiguous override of vf():
class MI : public D1, public D2 
{
public:
	const char* vf() const { return "MI"; }
};

int main() 
{
	vector<MBase*> vec;

	vec.push_back(new D1);
	vec.push_back(new D2);

	// Cannot upcast: which subobject?:
	//MBase* mi = new MI();
	D1* mi = new MI();
	vec.push_back(mi);

	for(size_t i = 0; i < vec.size(); i++)
		cout << vec[i]->vf() << endl;

	for(size_t i = 0; i < vec.size(); ++i)
		delete vec[i];
}
