#include <vector>
#include <iostream>

using namespace std;

struct Point
{
	int x, y;
	Point(int x = 0, int y = 0) : x(x), y(y)
	{}
};

class Polygon
{
public:
	Polygon() : area_(-1) {}

    void add_point(const Point& pt)
	{
		invalidate_area();
		points_.push_back(pt);
	}

    Point point(int i) const
	{								
		return points_[i];			
	}

    int number_of_points() const
	{
		return points_.size();
	}

    double area() const
	{
		if (area_ < 0) 
		{
			calc_area();
		}

		return area_;
	}

private:
	void invalidate_area() { area_ = -1; } 
	
    void calc_area() const
	{
		area_ = 0;
        vector<Point>::const_iterator i;
		for(i = points_.begin(); i != points_.end(); ++i)
			area_ += 1 /*jakieś obliczenia*/;
	}

	vector<Point> points_;
    mutable double area_;
};

Polygon operator+(const Polygon& lhs, const Polygon& rhs)
{ 
	Polygon result = lhs;
	int last = rhs.number_of_points(); 
	for(int i = 0; i < last; ++i) // konkatenacja
	{
		result.add_point(rhs.point(i));
	}

	return result;
}

void f(Polygon& pgn)
{
    pgn.add_point( Point(0, 0) );
}

void g(Polygon& pgn)
{
	pgn.add_point( Point(1, 1) );
}

void h(Polygon* ptr_pgn)
{
	ptr_pgn->add_point( Point(2, 2) );
}

int main()
{
	Polygon pgn;
	const Polygon const_pgn;

	f(pgn);
    //f(const_pgn);
	g(pgn);
	h(&pgn);
}
