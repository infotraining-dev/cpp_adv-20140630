// main.cpp
#include <iostream>
#include "DeviceWidget.h"
#include "Devices.h"

void do_stuff()
{
	using namespace hardware;
	using namespace ui;

	Device dev1;
	DeviceWidget widget1(dev1);

	hardware::Aux a1;
	use_aux(a1);
}


int main( ) 
{
	hardware::Device d;
	ui::DeviceWidget myWidget(d);
	
	// ...

	do_stuff();
}
