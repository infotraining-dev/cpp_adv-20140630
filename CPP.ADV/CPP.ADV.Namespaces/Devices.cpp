// Devices.cpp
#include "Devices.h"
#include <string>
#include <list>

namespace hardware {

	using std::string;
	using std::list;

	unsigned long Device::getUptime( ) const {
		return(uptime_);
	}

	string Device::getStatus( ) const {
		return(status_);
	}

	void DeviceMgr::getDeviceIds(list<string>& ids) const {
	}

	Device DeviceMgr::getDevice(const string& id) const {
		Device d;
		return(d);
	}

	void Aux::print() const
	{
			std::cout << "hardware::Aux.print()" << std::endl;
	}

	void use_aux(Aux a)
	{
		a.print();
	}
}
