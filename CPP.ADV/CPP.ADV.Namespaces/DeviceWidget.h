// DeviceWidget.h

#ifndef DEVICEWIDGET_H
#define DEVICEWIDGET_H
#include "Devices.h"

namespace ui {

	class Widget { /* ... */ };

	class DeviceWidget : public Widget {
	public:
		DeviceWidget(const hardware::Device& dev) : device_(dev) {}
		// reszta
	protected:
		hardware::Device device_;
	};

	class Aux 
	{
	public:
		void print() const
		{
			std::cout << "ui::Aux.print()" << std::endl;
		}
	};

	void use_aux(Aux a)
	{
		a.print();
	}
}

#endif // DEVICEWIDGET_H
