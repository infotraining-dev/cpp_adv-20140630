// Devices.h
#ifndef DEVICES_H__
#define DEVICES_H__
#include <string>
#include <iostream>
#include <list>

namespace hardware {

	class Device {
	public:
		Device( ) : uptime_(0), status_("nieznany") {}
		unsigned long getUptime( ) const;
		std::string getStatus( ) const;
		void reset( );
	private:
		unsigned long uptime_;
		std::string status_;
	};

	class DeviceMgr {
	public:
		void getDeviceIds(std::list<std::string>& ids) const;
		Device getDevice(const std::string& id) const;
		// reszta kodu...
	};

	class Aux 
	{
	public:
		void print() const;
	};

	void use_aux(Aux a);
}

#endif // DEVICES_H__
