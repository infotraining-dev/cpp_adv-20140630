#include <iostream>
#include <string>

#include "Matrix.hpp"


int main()
{
    {
        Matrix m(3);

        m[0][0] = 1;
        m[1][1] = 2;
        m[2][2] = 3;

        std::cout << "macierz m:\n" << m << std::endl;

        const Matrix cm(m);

        //cm[0][0] = 3;  // blad kompilacji!

        std::cout << "macierz cm:\n" << cm << std::endl;


        try
        {
            m.get_element(13, 2);
        }
        catch(const Matrix::OutOfIndex& e)
        {
            std::cout << e.what() << std::endl;
            std::cout << "index i = " << e.index_i() << "\n"
                      << "index j = " << e.index_j() << std::endl;
        }
        catch(const std::exception& e)
        {
            std::cout << e.what() << std::endl;
        }
        catch(...)
        {
            std::cout << "Ostatnia instancja catch! Zlapalem ale nie wiem co." << std::endl;
        }

        Matrix n(5);

        for(size_t i = 0; i < n.dim(); ++i)
            n[i][i] = i*i;

        std::cout << "\nmacierz n:\n" << n << std::endl;

        m = n;

    }

	//	std::cout << "macierz m po przypisaniu jej n:\n" << m << std::endl;*/
	//}
}
