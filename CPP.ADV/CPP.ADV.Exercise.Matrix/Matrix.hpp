#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <iostream>
#include <exception>
#include <stdexcept>
#include <string>
#include <algorithm>

class Matrix
{
    size_t dim_;
    int** matrix_;
public:
    Matrix(size_t dim) : dim_(dim)
    {
        alloc(dim_);

        init();
    }

    Matrix(const Matrix& source) : dim_(source.dim())
    {
        alloc(dim_);

        // kopiowanie
        for(size_t i = 0; i < dim_; ++i)
            std::copy(source.matrix_[i], source.matrix_[i] + dim_, matrix_[i]);
    }

    void swap(Matrix& other)
    {
        std::swap(matrix_, other.matrix_);
        std::swap(dim_, other.dim_);
    }

    Matrix& operator=(const Matrix& source)
    {
        Matrix temp(source);

        swap(temp);

        return *this;
    }

    ~Matrix()
    {
        for(size_t i = 0; i < dim_; ++i)
            delete [] matrix_[i];
        delete [] matrix_;
    }

    int* operator[](size_t index)
    {
        return matrix_[index];
    }

    const int* operator[](size_t index) const
    {
        return matrix_[index];
    }

    size_t dim() const
    {
        return dim_;
    }

    int& get_element(size_t index_i, size_t index_j)
    {
        if (index_i >= dim_ || index_j >= dim_)
            throw OutOfIndex("Index out of range", index_i, index_j);

        return matrix_[index_i][index_j];
    }

    const int& get_element(size_t index_i, size_t index_j) const
    {
        if (index_i >= dim_ || index_j >= dim_)
            throw OutOfIndex("Index out of range", index_i, index_j);

        return matrix_[index_i][index_j];
    }

private:
    void init()
    {
        for(size_t i = 0; i < dim_; ++i)
            std::fill_n(matrix_[i], dim_, 0);
    }

    void alloc(size_t dim)
    {
        matrix_ = new int*[dim];

        std::fill_n(matrix_, dim_, reinterpret_cast<int*>(NULL));

        try
        {
            for(size_t i = 0; i < dim; ++i)
                matrix_[i] = new int[dim];
        }
        catch(...)
        {
            this->~Matrix();

            throw;
        }
    }

public:
    class OutOfIndex : public std::out_of_range
    {
        size_t i_;
        size_t j_;
    public:
        OutOfIndex(const std::string& msg, size_t index_i, size_t index_j)
            : std::out_of_range(msg), i_(index_i), j_(index_j)
        {}

        size_t index_i() const
        {
            return i_;
        }

        size_t index_j() const
        {
            return j_;
        }
    };
};

std::ostream& operator<<(std::ostream& out, const Matrix& m)
{
    for(size_t row_index = 0; row_index < m.dim(); ++row_index)
    {
        out << "[ ";
        for(size_t column_index = 0; column_index < m.dim(); ++column_index)
            out << m[row_index][column_index] << " ";
        out << "]\n";
    }

    return out;
}

#endif
