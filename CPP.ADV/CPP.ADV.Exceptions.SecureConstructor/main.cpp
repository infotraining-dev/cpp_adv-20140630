#include <iostream>
#include <cstdlib>
#include <stdexcept>

using namespace std;

/********************************************************************
*  Uodparnianie konstrukora na wyjatki #1
********************************************************************/

class Device
{
private:
	size_t devno_;
public:
	Device(int devno) : devno_(devno)
	{
		if (devno == 2)
			throw std::runtime_error("Powazny problem!");

		cout << "Konstruktor Device #" << devno << endl;
	}

	~Device()
	{
		cout << "Destruktor Device #" << devno_ << endl;
	}

};

class Broker 
{
public:
	Broker(int devno1, int devno2) : dev1_(NULL), dev2_(NULL)
	{
		// TODO - poprawa odpornosci na wyjatki
        dev1_ = new Device(devno1);  // 1
        try
        {
            dev2_ = new Device(devno2);  // 2
        }
        catch(...)
        {
            delete dev1_;
            throw;
        }
	}

	~Broker()
	{
		delete dev1_;
		delete dev2_;
	}
private:
	Device* dev1_;
	Device* dev2_;
};

int main()
{
	try
	{
		Broker b(1, 2);
	}
	catch(const exception& e)
	{
		cerr << "Wyjatek: " << e.what() << endl;
	}
}
