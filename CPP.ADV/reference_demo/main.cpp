#include <iostream>
#include <list>

using namespace std;

class Value
{
    int value_;
public:
    Value(int value = 0) : value_(value)
    {
    }

    int value() const
    {
        return value_;
    }

    // preinkrementacja
    Value& operator++()
    {
        ++value_;

        return *this;
    }

    // postinkrementacja
    const Value operator++(int)
    {
        Value temp(*this);

        ++value_;

        return temp;
    }
};

void other(string& text)
{
    text += "Other";
}

void foo(const string& text)
{
    cout << "foo: " << text << endl;

    other(const_cast<string&>(text)); // potencjalnie UB
}

int main()
{
    list<int> lst;

    lst.push_back(1);
    lst.push_back(2);
    lst.push_back(3);

    for(list<int>::const_iterator it = lst.begin(); it != lst.end(); ++it)
        cout << *it << " ";
    cout << endl;

    char c;

    char ctxt[] = "text";

    char (&ref_ctext)[5] = ctxt;

    ref_ctext[0] = 'T';

    cout << ctxt << endl;

    string txt = "Hello";
    const string const_txt = "Const Hello";

    // thread1
    foo(txt);
    //foo(const_txt); // UB

    // thread2
    other(txt);  // race condition

    foo(string("Hello"));

    other((string&)(const_txt));  // UB

    cout << const_txt << endl;

    Value v(1);

    ++++++v;

    cout << v.value() << endl;

    v++;

    cout << v.value() << endl;

    int x = 1;
}

