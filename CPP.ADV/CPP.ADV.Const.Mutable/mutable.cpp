#include <iostream>


#include "time.hpp"

using namespace std;

void use_time(const Time& t)
{
	cout << "local time: " << t.to_string() << endl;
}

int main()
{
	try
	{
		Time t1(22, 58, 21);

		use_time(t1);
		use_time(t1);

		t1.set_hours(23);

		use_time(t1);
		use_time(t1);

		t1.set_minutes(44);

		use_time(t1);
		use_time(t1);

		t1.set_seconds(59);

		use_time(t1);
		use_time(t1);
	}
	catch(const exception& e)
	{
		cout << e.what() << endl;
	}
}
