#ifndef TIME_HPP
#define TIME_HPP

#include <string>
#include <mutex>

class Time
{	
public:
	Time(int h = 0, int m = 0, int s = 0);

	int hours() const;
	void set_hours(int h);
	int minutes() const;
	void set_minutes(int m);
	int seconds() const;
	void set_seconds(int s);

	std::string to_string() const;  // TODO: zoptymalizować efektywność tej metody - cache + lazy evaluation
private:
	int hours_, minutes_, seconds_;
    mutable std::mutex cached_mutex_;
    mutable bool is_cache_valid_;
    mutable std::string cached_string_rep_;

    void calulate_cached_string_rep() const;

	static bool is_valid(int h = 0, int m = 0, int s = 0);
};

inline int Time::hours() const
{
	return hours_;
}

inline int Time::minutes() const
{
	return minutes_;
}

inline int Time::seconds() const
{
	return seconds_;
}

#endif
