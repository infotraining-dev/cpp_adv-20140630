#include "time.hpp"
#include <sstream>
#include <iomanip>
#include <stdexcept>
#include <mutex>

using namespace std;

Time::Time(int h, int m, int s)
    : hours_(h), minutes_(m), seconds_(s), is_cache_valid_(false)
{
	if (!is_valid(h, m, s))
		throw std::invalid_argument("Nieprawidlowe argumenty konstruktora Time.");
}

bool Time::is_valid(int h, int m, int s)
{
	if ((h < 0) || (h > 23))
		return false;
	if ((m < 0) || (m > 59))
		return false;
	if ((s < 0) || (s > 59))
		return false;

	return true;
}

void Time::set_hours(int hours)
{
	if (!is_valid(hours))
		throw std::invalid_argument("Nieprawidlowa wartosc godzin.");

	hours_ = hours;

    std::lock_guard<std::mutex> lk(cached_mutex_);
    is_cache_valid_ = false;
}

void Time::set_minutes(int minutes)
{
	if (!is_valid(0, minutes))
		throw std::invalid_argument("Nieprawidlowa wartosc minut.");

	minutes_ = minutes;

    std::unique_lock<std::mutex> lk(cached_mutex_);
    is_cache_valid_ = false;

    lk.unlock();

    // inny kod
}

void Time::set_seconds(int seconds)
{
	if (!is_valid(0, 0, seconds))
		throw std::invalid_argument("Nieprawidlowa wartosc sekund.");

	seconds_ = seconds;

    std::lock_guard<std::mutex> lk(cached_mutex_);
    is_cache_valid_ = false;
}

void Time::calulate_cached_string_rep() const
{
    string result;
    stringstream out(result);

    out.fill('0');
    out << setw(2) <<  hours_ << ":" << setw(2) << minutes_ << ":"  << setw(2) << seconds_;

    cached_string_rep_ =  out.str();
}

string Time::to_string() const
{
    std::lock_guard<std::mutex> lk(cached_mutex_);

    if (!is_cache_valid_)
    {
        calulate_cached_string_rep();
        is_cache_valid_ = true;
    }

    return cached_string_rep_;
}
