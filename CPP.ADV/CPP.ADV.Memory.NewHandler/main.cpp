#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <new>

using namespace std;

void no_memory_handler()
{
	std::cout << "No memory handler!" << std::endl;
	
	throw bad_alloc();
}

int main()
{
	set_new_handler(no_memory_handler);

	try
	{
		while ( 1 ) 
		{
			new int[5000000];
			
			cout << "Allocating 5000000 ints." << endl;
		}
	}
	catch ( const std::exception& e )
	{
		cout << e.what( ) << endl;
	}
}
