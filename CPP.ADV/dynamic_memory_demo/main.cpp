#include <iostream>
#include <cstdlib>
#include <vector>

using namespace std;

class Value
{
    int value_;
public:
    Value(int value = 0) : value_(value)
    {
        //cout << "Value(" << value_ << ")" << endl;
    }

    ~Value()
    {
        //cout << "~Value(" << value_ << ")" << endl;
    }

    int value() const
    {
        return value_;
    }

    void set_value(int value)
    {
        value_ = value;
    }

    // preinkrementacja
    Value& operator++()
    {
        ++value_;

        return *this;
    }

    // postinkrementacja
    const Value operator++(int)
    {
        Value temp(*this);

        ++value_;

        return temp;
    }

    static void* operator new(size_t size)
    {
        cout << "Value::new" << endl;
        return malloc(size);
    }

    static void operator delete(void* ptr)
    {
        cout << "Value::delete" << endl;
        free(ptr);
    }
};

class NoHeap
{
public:
    int value;
private:
    void* operator new(size_t);
    void operator delete(void*);
};


class NoStack
{
public:
    int value;

    void destroy()
    {
        delete this;
    }

private:
    ~NoStack() {}
};


int main()
{
//    // 1 - alokacja pamieci
//    void* raw_mem = ::operator new(sizeof(Value));

//    // 2 - inicjalizacja - placement new
//    Value* pv0 = new (raw_mem) Value(10);

//    cout << "pv0: " << pv0->value();

//    // 3 - wywolanie destruktora
//    pv0->~Value();

//    // 4 - zwolnienie pamieci
//    ::operator delete(raw_mem);


    //vector<Value> vec(100000000000);

    Value* pv1 = new Value(1);

    cout << "*pv1: " << pv1->value() << endl;

    delete pv1;

    cout << "\n\n-----------------\n" << endl ;

    pv1 = new (nothrow) Value[10];

    if (!pv1)
    {
        cout << "Brak pamięci!!!" << endl;
        exit(1);
    }

    int counter = 0;
    for(Value* it = pv1; it != pv1 + 10; ++it)
        it->set_value(++counter);

    delete [] pv1;

    NoHeap nh1;

    nh1.value = 19;

    cout << "nh1: " << nh1.value << endl;

    NoStack* pns = new NoStack;

    pns->value = 19;

    pns->destroy();
}

