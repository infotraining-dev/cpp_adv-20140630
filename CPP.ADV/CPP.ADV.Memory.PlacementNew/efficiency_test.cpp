//#include <windows.h> // gcc...
#include <stdio.h>
#include <chrono>
#include <string>
#include <iostream>

using namespace std;

class NoDefaultConstructor
{
public:
	NoDefaultConstructor(const std::string m) : member_(m)
	{
	}

	~NoDefaultConstructor() {}

private: 
	std::string member_;
};

inline void efficiency_test()
{
    const size_t SIZE = 1000000;

	auto start = std::chrono::high_resolution_clock::now();
		
	NoDefaultConstructor* objects[SIZE];

    for (size_t i = 0; i < SIZE; ++i)
	{
		objects[i] = new NoDefaultConstructor("Test");
	}

    for (size_t i = 0; i < SIZE; ++i)
		delete objects[i];

	auto end = std::chrono::high_resolution_clock::now();
    float elapsedSeconds = std::chrono::duration_cast<std::chrono::duration<float>>(end-start).count();
	printf("Program took: %f seconds\n", elapsedSeconds);

	cout << "\n//----------------------------\n\n";

	start = std::chrono::high_resolution_clock::now();

	// rezerwacja pamięci
	NoDefaultConstructor* ptr = reinterpret_cast<NoDefaultConstructor*>(::operator new(sizeof(NoDefaultConstructor) * SIZE));
	
    for (size_t i = 0; i < SIZE; ++i)
	{
		new (ptr+i) NoDefaultConstructor("Test");
	}

    for (size_t i = 0; i < SIZE; ++i)
		ptr[i].~NoDefaultConstructor();

	::operator delete(ptr);


	end = std::chrono::high_resolution_clock::now();
    elapsedSeconds = std::chrono::duration_cast<std::chrono::duration<float>>(end-start).count();
	printf("Program took: %f seconds\n", elapsedSeconds);
}
