#include <iostream>
#include <cstdlib>
#include <string>

#include "efficiency_test.cpp"

class Employee
{
private:
	const int id_;
	std::string name_;

public:
	Employee(int id, const std::string& name) : id_(id)
	{
		name_ = name;
	}

	~Employee() {}

	int get_id() const
	{
		return id_;
	}

	std::string get_name() const
	{
		return name_;
	}

	void set_name(const std::string& name)
	{
		name_ = name;
	}
};

std::ostream& operator<<(std::ostream& out, const Employee& e)
{
	out << "Id: " << e.get_id() << " Name: " << e.get_name();

	return out;
}

void placement_new_demo()
{
	// rezerwacja surowej pamieci
	void* ptrRawMem = ::operator new(sizeof(Employee));

	// umieszczajacy operator new
	Employee* ptrEmp = new (ptrRawMem) Employee(1, "Jan Kowalski");

	std::cout << *ptrEmp << std::endl;

	// jawna desktrukcja obiektu
	ptrEmp->~Employee();

	// zwolnienie pamieci
	::operator delete(ptrRawMem);
}

int main()
{
	//placement_new_demo();
	efficiency_test();
}
