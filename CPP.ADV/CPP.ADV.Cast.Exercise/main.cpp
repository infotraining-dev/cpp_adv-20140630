class A
{ 
public:
    A() {}
	virtual ~A() {}; 
};

class B : private virtual A { /*...*/ };

class C : public A { /*...*/ };

class D : public B, public C { /*...*/ };

// zmienne
A a1; 
B b1;
C c1;
D d1;
const A a2;
const A& ra1 = a1;
const A& ra2 = a2;
char c;

/* zadanie 1 - które z następujących rzutowań nie są równoważne żadnym rzutowaniom w C */
// const_cast
// dynamic_cast
// reinterpret_cast
// static_cast

/* zadanie 2 - dla każdego z następujących rzutowań w C napisz równoważne rzutowanie nowego typu.
			   Które z nich są niepoprawne? */

void f()
{
    A* pa; B* pb; C* pc;

    pa = (A*)&ra1; // const_cast<A*>(&ra1)
    //pa = (A*)&a2;  // nie można wyrazić nowym sposobem rzutowania - ponieważ a2 jest stałą efekt jest nieokreślony
    pb = (B*)&c1;  // reinterpret_cast<B*>(&c1)
    pc = (C*)&d1;  // pc = &d1 - w c++ żadne rzutowanie nie jest konieczne
}


/* zadanie 3 - oceń styl i poprawność każdego z następujących rzutowań */
void g()
{
    //unsigned char* puc = static_cast<unsigned char*>(&c); // błąd! reinterpret_cast<unsigned char*>
    void* pv = static_cast<void*>(&b1); // ok - ale niepotrzebne
    B* pb1 = static_cast<B*>(pv); // ok
    B* pb2 = &b1; // ok - ale niepotrzebne
    A* pa1 = const_cast<A*>(&ra1); // ok
    B* pb3 = dynamic_cast<B*>(&c1); // błąd! typ C nie jest pochodny po B
    //A* pa3 = dynamic_cast<A*>(&b1); // błąd! b1 nie jest typu A
    B* pb4 = static_cast<B*>(&d1); // ok - ale niepotrzebne
    D* pd = static_cast<D*>(pb4); // ok
    //pa1 = dynamic_cast<A*>(pb2); // błąd!
    //pa1 = dynamic_cast<A*>(pb4); // błąd!
    C* pc1 = dynamic_cast<C*>(pb4); // ok
    C& rc1 = dynamic_cast<C&>(*pb2); // błąd! rzucony wyjątek
}

int main()
{
    f();
}
	
		
