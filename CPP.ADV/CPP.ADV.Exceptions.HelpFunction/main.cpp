#include <iostream>
#include <cstdlib>
#include <exception>
#include <stdexcept> 

class Array
{
    size_t size_;
	int* buffer_;
	
public:
	Array(size_t size) : size_(size)
	{
		buffer_ = new int[size_];
	}
	
	~Array()
	{
		delete [] buffer_;
	}

    Array(const Array& source) : size_(source.size_), buffer_(NULL)
    {
        buffer_ = new int[size_];
        std::copy(source.buffer_, source.buffer_ + size_, buffer_);
    }

    void swap(Array& other)
    {
        std::swap(buffer_, other.buffer_);
        std::swap(size_, other.size_);
    }

    Array& operator=(const Array& source)
    {
        Array temp(source);
        swap(temp);

        return *this;
    }

    size_t size() const
    {
        return size_;
    }
	
	int& at(size_t index)
	{
		if (index >= size_)
			throw IndexOutOfRange("Index is out of range", index);
			
		return buffer_[index];	
	}

    const int& at(size_t index) const
    {
        if (index >= size_)
            throw IndexOutOfRange("Index is out of range", index);

        return buffer_[index];
    }
	
	class IndexOutOfRange : public std::out_of_range
	{
		size_t index_;
	public:
		IndexOutOfRange(const std::string& msg, size_t index) : std::out_of_range(msg), index_(index)
		{
		}

        const char* what() const throw()
		{
			return std::out_of_range::what();
		}

		size_t get_index() const
        {
			return index_;
		}
	};
};

using namespace std;

void process_exception()
{
	try
	{
		throw;
	}
	catch(const Array::IndexOutOfRange& e)
	{
		cout << e.what() << endl;
		cout << "Invalid index value: " << e.get_index() << endl;
	}
	catch(const std::bad_alloc& e)
	{
		cout << e.what() << endl;
	}	
	catch(const std::exception& e)
	{
		cout << e.what() << endl;
	}		
}

void myTerminate()
{
	cout << "I'll be back!" << endl;
	
	std::abort();
}

int main()
{
    Array a0(10);

    for(size_t i = 0; i < a0.size(); ++i)
        a0.at(i) = i;


    try
    {
        Array a2(20);

        a2 = a0;

        for(size_t i = 0; i < a2.size(); ++i)
            cout << a2.at(i) << " ";
        cout << endl;
    }
    catch(...)
    {

    }




	std::set_terminate(myTerminate);
	
	// bez funkcji pomocniczej
	try
	{	
		Array a1(10);		
		a1.at(0) = 1;		
		cout << a1.at(0) << endl;			
		a1.at(11); // blad
	}
	catch(const std::out_of_range& e)
	{
		cout << e.what() << endl;
	}
	catch(const std::bad_alloc& e)
	{
		cout << e.what() << endl;
	}	
	catch(const std::exception& e)
	{
		cout << e.what() << endl;
	}

	// z funkcja pomocnicza
	try 
	{
		Array a2(10);		
		a2.at(0) = 1;		
		cout << a2.at(0) << endl;			
		a2.at(11); // blad
	}
	catch(...)
	{
        process_exception();
	}
}
