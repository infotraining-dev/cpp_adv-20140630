#include <iostream>
#include <cstdlib>
#include <list>

class Handle
{
public:
	virtual ~Handle() {};
	void* operator new(size_t);
	void operator delete(void*);
};

struct rep
{
	static const size_t max = 10;
	static rep* free;  // czoło listy bloków wolnych
	static size_t num_used;  // licznik bloków zajętych
	union
	{
		char store[sizeof(Handle)];
		rep* next;
	};
};

static rep mem[rep::max];   // blok pamięci statycznej
rep* rep::free = NULL;
size_t rep::num_used = 0;

void* Handle::operator new(size_t)
{
	std::cout << "Dziala Handle::operator new\n";

	if ( rep::free ) // jeśli lista nie jest wyczerpana
	{
		rep* tmp = rep::free;  // bierzemy z listy
		rep::free = rep::free->next;
		return tmp;
	}
	else if ( rep::num_used < rep::max ) // jeśli zostały jeszcze wolne bloki
		return &mem[rep::num_used++];	 // zwracamy niewykorzystany slot
	else
		throw std::bad_alloc();  // koniec pamięci
}

void Handle::operator delete(void* p)  // dodanie bloku do listy wolnych bloków
{
	std::cout << "Dziala Handle::operator delete\n";

	static_cast<rep*>(p)->next = rep::free;
	rep::free = static_cast<rep*>(p);
}


int main()
{
	std::cout << "&mem: " << (size_t)&mem << std::endl;

	Handle* ph1 = new Handle();
	Handle* ph2 = new Handle();
	Handle* ph3 = new Handle();

	std::cout << "ph1: " << (size_t)ph1 << std::endl;
	std::cout << "ph2: " << (size_t)ph2 << std::endl;
	std::cout << "ph3: " << (size_t)ph3 << std::endl;

	delete ph3; 
	delete ph2;
	
	
	std::cout << "Ponowny przydział pamięci:\n";

	ph2 = new Handle();
	std::cout << "ph2: " << (size_t)ph2 << std::endl;
	ph3 = new Handle();
	std::cout << "ph3: " << (size_t)ph3 << std::endl;
	Handle* ph4 = new Handle();
	std::cout << "ph4: " << (size_t)ph4 << std::endl;

	std::list<Handle*> lst;
	try
	{
		for(int i = 0; i < 10; ++i)
			lst.push_back(new Handle());
	}
	catch(const std::bad_alloc& e)
	{
		std::cout << e.what() << std::endl;
		std::list<Handle*>::iterator lit = lst.begin();
		std::list<Handle*>::iterator end = lst.end();
		for(; lit != end; ++lit)
			delete *lit;
	}

	delete ph4;
	delete ph3;
	delete ph2;
	delete ph1;
}
