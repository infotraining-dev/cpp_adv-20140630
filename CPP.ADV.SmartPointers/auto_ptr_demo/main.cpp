#include <iostream>
#include <stdexcept>
#include <memory>
#include <cassert>
#include <vector>
#include <boost/scoped_ptr.hpp>
#include <boost/scoped_array.hpp>

using namespace std;

class Value
{
    int value_;
public:
    Value(int value = 0) : value_(value)
    {
        cout << "Value(" << value_ << ")" << endl;
    }

    ~Value()
    {
        cout << "~Value(" << value_ << ")" << endl;
    }

    int value() const
    {
        return value_;
    }

    void set_value(int value)
    {
        value_ = value;
    }
};

void may_throw(int arg)
{
    if (arg % 2 == 0)
        throw runtime_error("Error");
}

Value* create_value(int v)
{
    return new Value(v);
}

auto_ptr<Value> safe_create_value(int v)
{
    return auto_ptr<Value>(new Value(v));
}

// przekazanie przez wartosc jako parametr zwolni zasob przy wyjsciu z funkcji
void do_stuff(auto_ptr<Value> ptr)
{
    cout << "do_stuff with value: " << ptr->value() << endl;
}

void f1()
{
    auto_ptr<Value> value(new Value(1));

    (*value).set_value(10);

    cout << "value = " << value->value() << endl;

    value.reset(new Value(15));

    // kopiowanie

    auto_ptr<Value> v2 = value;

    assert(value.get() == NULL);

    cout << "v2 = " << v2->value() << endl;

    may_throw(2);

    Value* ptr_value = v2.release();

    delete ptr_value;
}

void f2()
{
    auto_ptr<Value> v = safe_create_value(12);

    do_stuff(v);

    cout << "Value: " << v->value() << endl;

    safe_create_value(13);

    may_throw(2);
}


unique_ptr<Value> safe_create_value2(int v)
{
    return unique_ptr<Value>(new Value(v));
}

void f3()
{
    unique_ptr<Value> v1(new Value(15));

    cout << "v1 = " << v1->value() << endl;

    unique_ptr<Value> v2 = move(v1);

    cout << "v2 = " << v2->value() << endl;

    assert(v1.get() == 0);

    unique_ptr<Value> v3 = safe_create_value2(19);

    vector<unique_ptr<Value>> vec;

    vec.push_back(unique_ptr<Value>(new Value(22)));
    vec.push_back(safe_create_value2(23));
    vec.push_back(move(v2));
    vec.push_back(move(v3));

    for(auto& ptr : vec)
    {
        cout << "Value: " << ptr->value() << endl;
    }

    unique_ptr<Value[]> array(new Value[10]);

    array[1].set_value(99);
}

void f4()
{
    boost::scoped_ptr<Value> v(new Value(1));

    may_throw(2);

    boost::scoped_array<Value> array(new Value[10]);

    array[0].set_value(100);
}

int main() try
{
    //f1();
    //f2();
    f4();
}
catch(...)
{
    cout << "Main catch... End of app...\n";
}
